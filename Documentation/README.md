# README #

### Subject of the repository ###

* This project aims at defining different kinds of investment strategy. 
It consists in defining the classes that we would need for investment which are the financial instruments that an investor would have the ability to choose from whhen creating his portfolio.
Then, three different types of investors will be created ( aggressive, defensive,mixed)
Finally, a simulation part will be created in order to use create different investors and different portfolios that are related to them. 

### How do I get set up? ###

* Packages that need to be installed are: 
* pandas
* numpy
* os
* date
* matplotlib.pyplot

 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Authors ###

* Oumaima Hachicha / e-mail: oumaima.hachicha@tsm-education.fr
* Hasnaa Afttah / e-mail: hasnaa.afttah@tsm-education.fr