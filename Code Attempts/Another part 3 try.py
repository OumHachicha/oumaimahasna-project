import pandas_datareader.data as web
import matplotlib.pyplot as plt
import datetime
import numpy as np
import pandas as pd

from pandas import Series, DataFrame

# Getting the nearest business day to the date we have
def nearest(self, df, date):
    return min(df, key=lambda x: abs(x - date))



# from yahoo we get the data
start = datetime.datetime(2016, 9, 1)
end = datetime.datetime(2021, 1, 1)

df = web.DataReader(("IBM", "FDX", "GOOGL","XOM", "KO", "NOK", "MS"), 'yahoo', start, end)
df.tail()


# choosing the 'High' column for the prices of our stocks
Price = df['High']

stocks = ['IBM', 'FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS']
# download daily price data for each of the stocks in the portfolio
data = web.DataReader(stocks, data_source='yahoo', start='01/09/2016')['Adj Close']
data.sort_index(inplace=True)
# convert daily stock prices into daily returns
returns = data.pct_change()
# calculate mean daily return and covariance of daily returns
mean_daily_returns = returns.mean()
cov_matrix = returns.cov()

## Creating the stock class
class Stocks(object):
    def __init__(self, t, amount, name, number_of_stocks_bought, purchase_date):
        self.t = t
        self.amount = amount
        self.number_of_stocks_bought = number_of_stocks_bought
        self.name = ['FDX', 'GOOGL', 'XOM', 'KO','NOK', 'MS', 'IBM']
        self.purchase_date = purchase_date

    def purchase_date_accepted(self):
        if self.purchase_date in data['Date']:
            return("This date is available")
        else:
            return ValueError("Enter a valid purchase date")

All_the_stocks = pd.concat(['GOOGL', 'XOM', 'KO', 'NOK','MS', 'IBM'], axis=0, sort=False)

    # date settings
def date_settings(self, date):
    if date in data['date']:
          return ('valid date')
    elif date not in data['date']:
        return date == nearest(data, date)

def get_the_price(self, name, start_date, end_date):
    start_date = input(print('Give a start date for the investment'))
    end_date = input(print('Give an end date for your investment'))
    if (start_date in data['date']) and (end_date in data['date']):
        price_start = All_the_stocks[(All_the_stocks['DATE'] == start_date) & (All_the_stocks['NAME'] == name)]
        price_end = All_the_stocks[(All_the_stocks['DATE'] == end_date) & (All_the_stocks['NAME'] == name)]
        return price_start & price_end
    elif (start_date in df['date']) or (end_date in df['date']):
         start_date = nearest(df, start_date)
         end_date = nearest(df, end_date)
         price_start = All_the_stocks((All_the_stocks['DATE'] == start_date) & (All_the_stocks['NAME'] == name))
         price_end = All_the_stocks((All_the_stocks['DATE'] == end_date) & (All_the_stocks['NAME'] == name))
         return price_start & price_end

# set number of runs of random portfolio weights
num_portfolios = 25000
# set up array to hold results
# We have increased the size of the array to hold the weight values for each stock
results = np.zeros((3 + len(stocks) - 1, num_portfolios))
for i in range(num_portfolios):
# select random weights for portfolio holdings
    weights = np.array(np.random.random(7))
# rebalance weights to sum to 1
    weights = np.sum(weights)

    # calculate portfolio return and volatility
    portfolio_return = np.sum(mean_daily_returns * weights) * 252
    portfolio_std_dev = np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights))) * np.sqrt(252)

    # store results in results array
    results[0, i] = portfolio_return
    results[1, i] = portfolio_std_dev
    # store Sharpe Ratio (return / volatility) - risk free rate element excluded for simplicity
    results[2, i] = results[0, i] / results[1, i]
    # iterate through the weight vector and add data to results array
    for j in range(len(weights)):
        results[j + 6, i] = weights[j]
# convert results array to Pandas DataFrame
results_frame = pd.DataFrame(results.T, columns=['ret', 'stdev', 'sharpe', stocks[0], stocks[1], stocks[2], stocks[3],stocks[4], stocks[5], stocks[6]])
# locate position of portfolio with highest Sharpe Ratio
max_sharpe_port = results_frame.iloc[results_frame['sharpe'].idxmax()]
# locate positon of portfolio with minimum standard deviation
min_vol_port = results_frame.iloc[results_frame['stdev'].idxmin()]
# create scatter plot coloured by Sharpe Ratio
plt.scatter(results_frame.stdev, results_frame.ret, c=results_frame.sharpe, cmap='RdYlBu')
plt.xlabel('Volatility')
plt.ylabel('Returns')
plt.colorbar()
# plot red star to highlight position of portfolio with highest Sharpe Ratio
plt.scatter(max_sharpe_port[1], max_sharpe_port[0], marker=(5, 1, 0), color='r', s=1000)
# plot green star to highlight position of minimum variance portfolio
plt.scatter(min_vol_port[1], min_vol_port[0], marker=(5, 1, 0), color='g', s=1000)
print(max_sharpe_port)
print(min_vol_port)