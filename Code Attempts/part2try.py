import pandas as pd
import datetime
import pandas_datareader.data as web
from pandas import Series, DataFrame
import matplotlib.pyplot as plt

# from yahoo we get the data
start = datetime.datetime(2016, 9, 1)
end = datetime.datetime(2021, 1, 1)

df = web.DataReader(("IBM", "FDX", "GOOGL","XOM", "KO", "NOK", "MS"), 'yahoo', start, end)
df.tail()
print(df)

# choosing the 'High' column for the prices of our stocks
Price = df['High']
mavg = Price.rolling(window=100).mean()

# calculate the return
rets = Price / Price.shift(1) - 1
rets.plot(label='return')
plt.show()

# i tried this for the portfolio, it still doesnt work :/

# import datetime
# from datetime import timedelta
#
# class Portfolio():
#
#
#     def __init__(self, Price, q, startdate, enddate):
#         df('Symbols').__init__(self, Price, q, startdate, enddate)
#         self.Price = Price
#         self.q = q
#         self.startdate = datetime.date(year, month, day)
#         self.enddate = datetime.date(year, month, day)
# year = int(input('Enter a year'))
# month = int(input('Enter a month'))
# day = int(input('Enter a day'))
# q = int(input("Enter the quantity of stocks bought "))
#
# def Portfolio_return(self):
#     return self.Price / Price.shift(1) - 1
