class Bond(object):
    def __init__(self, min_price, amount, t):
        self.min_price = min_price
        self.amount = amount
        self.t = t


# The term creation
class Term(Bond):
    SHORT = 'SHORT'
    LONG = 'LONG'

    def __init__(self, type, min_price, amount, t):
        Bond.__init__(self, min_price, amount, t)
        if type == Term.LONG:
            self.min_amount = 1000
            self.min_t = 5
            self.r = 1.5 / 100
        elif type == Term.SHORT:
            self.min_amount = 200
            self.min_t = 2
            self.r = 3 / 100

    def compounded_interest_rate(self):
        return self.amount * (((1 + self.r) ** self.t) - 1)


