import pandas as pd
import datetime
import pandas_datareader.data as web
from pandas import Series, DataFrame
import matplotlib.pyplot as plt


start = datetime.datetime(2016, 9, 1)
end = datetime.datetime(2021, 1, 1)

df = web.DataReader(("IBM", "FDX", "GOOGL","XOM", "KO", "NOK", "MS"), 'yahoo', start, end)
df.tail()
print(df)

df_daily_returns = df['High'].pct_change()
df_monthly_returns = df['High'].resample('M').ffill().pct_change()

print(df_daily_returns.head())
df_daily_returns.plot(label='daily return')
plt.show()
# cumulative returns
df_cum_returns = (df_daily_returns + 1).cumprod()
df_cum_returns.plot(label='cumulative returns')
plt.show()

fig = plt.figure()
ax1 = fig.add_subplot(321)
ax2 = fig.add_subplot(322)
ax3 = fig.add_subplot(323)
ax4 = fig.add_subplot(324)
ax5 = fig.add_subplot(325)
ax6 = fig.add_subplot(326)



ax1.plot(df['High']['IBM'])
ax1.set_title("IBM")
ax2.plot(df['High']['FDX'])
ax2.set_title("FedEx")
ax3.plot(df['High']['GOOGL'])
ax3.set_title("Google")
ax4.plot(df['High']['XOM'])
ax4.set_title("ExxonMobil")
ax5.plot(df['High']['NOK'])
ax5.set_title("Norwegian krone")
ax6.plot(df['High']['KO'])
ax6.set_title("The Coca-Cola Company")

plt.tight_layout()
plt.show()

# i didnt know how to specify the period of investment of the amount invested on each stock, i ll keep working on it