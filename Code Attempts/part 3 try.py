import datetime
import pandas_datareader.data as web
import numpy as np
import pandas as pd
import pandas_datareader.data as web
import matplotlib.pyplot as plt

from pandas import Series, DataFrame
import matplotlib.pyplot as plt

# from yahoo we get the data
start = datetime.datetime(2016, 9, 1)
end = datetime.datetime(2021, 1, 1)

df = web.DataReader(("IBM", "FDX", "GOOGL","XOM", "KO", "NOK", "MS"), 'yahoo', start, end)
df.tail()
print(df)

#* Aggressive investor

# choosing the 'High' column for the prices of our stocks
Price = df['High']

df.sort_index(inplace=True)
#convert daily stock prices into daily returns
returns = df.pct_change()
#convert daily stock prices into daily returns
#returns = df.pct_change()
#calculate mean daily return and covariance of daily returns
mean_daily_returns = returns.mean()
cov_matrix = returns.cov()

#set number of runs of random portfolio weights
num_portfolios = 25000
#set up array to hold results
results = np.zeros((3,num_portfolios))
for i in range(num_portfolios):
#select random weights for portfolio holdings
    weights = np.random.random(7)
# rebalance weights to sum to 1
    weights /= np.sum(weights)

# calculate portfolio return and volatility
rets = Price / Price.shift(1) - 1
portfolio_return = np.sum(mean_daily_returns * weights) * 252
portfolio_std_dev = np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights)))
np.sqrt(252)

#store results in results array
results[0,i] = portfolio_return
results[1,i] = portfolio_std_dev
#store Sharpe Ratio (return / volatility) - risk free rate element excluded for simplicity
results[2,i] = results[0,i] / results[1,i]
#convert results array to Pandas DataFrame
results_frame = pd.DataFrame(results.T,columns=['ret','stdev','sharpe'])
#create scatter plot coloured by Sharpe Ratio
plt.scatter(results_frame.stdev,results_frame.ret,c=results_frame.sharpe,cmap='RdYlBu')
plt.colorbar()

amount = int(input("Enter starting principle please. "))
n = int(input("Enter number of compounding periods per year. "))
i = float(input("Enter annual interest rate. e.g. 15 for 15% "))
t = int(input("Enter the amount of years. "))

FV = amount * (((1 + ((i/100.0)/n)) ** (n*t)))

print ("The final amount after", t, "years is", FV)