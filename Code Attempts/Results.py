import BondsClasses as bd
import matplotlib.pyplot as plt
import numpy as np
import os
# Creating two different types of bonds
# Trying to see if the code works
# Creating two different types of bonds
# short term and long term
# Trying to see if the code works
Bond1 = bd.Term(bd.Term.LONG, 1000, 3, 20)
print(Bond1.r)
print(Bond1.compounded_interest_rate())
Bond2 = bd.Term(bd.Term.SHORT, 200, 20, 3)
print(Bond2.r)
print(Bond2.compounded_interest_rate())


# Plots for the evolution of the min invested amount
import numpy as np
import matplotlib.pyplot as plt
import os
# For long term bond
principal_amount = 1000
no_of_years = 50
r = 3
compound_interest = list()
amount = 0
for i in range(no_of_years):
    amount += principal_amount+principal_amount*(r/100)
    principal_amount = amount
    compound_interest.append(amount)
    amount = 0
years = np.arange(1, no_of_years+1)+1970
plt.plot(years, compound_interest)
plt.xlabel('Years')
plt.ylabel('Amount')
plt.savefig(os.path.abspath('../Results/Min_Amount_Invested_Evolution.png'))
plt.show()