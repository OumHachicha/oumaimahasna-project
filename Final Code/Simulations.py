from Investor import strat_defensive, strat_aggressive, strat_mixed
from Bonds import Term.LONG, Term.SHORT
from Stocks import
from datetime import date
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import datetime
import random
# characteristics of financial assets
S_min_amount = 250
term_short = 2
rate_short = 0.015
L_min_amount= 1000
term_long = 5
rate_long = 0.03
initial_budget = 5000
Stocks = ['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']
# Importing the data
Data_imported = yf.download(All_tickers, start='2016-01-09', end='2021-01-01')['High']
## Modelisation of 500 investors from 2016 to 2020
prob_bonds = random.randint(0,100)/100
datelist = Data_imported['Date'].tolist()  #Choose a random date for the investment

year = []
list_of_defensive_investors = []
list_of_aggressive_investors = []
list_of_mixed_investors = []
models1 = (100)
models2 = (100)
models3 = (100)
models4 = (100)
models5 = (100)

# investment made in 2016, return for one year
for i in range(models1):
    year.append('2016')
    list_of_defensive_investors.append(strat_defensive(initial_budget, '2016-01-11', '2016-12-30'))
    list_of_aggressive_investors.append(strat_aggressive(initial_budget, '2016-01-11', '2016-12-30'))
    list_of_mixed_investors.append(strat_mixed(0.25, 0.75, initial_budget, '2016-01-11', '2016-12-30'))

# investment made from 2016 to 2017, Return for 2 years
for i in range(models2):
    year.append('2017')
    list_of_defensive_investors.append(strat_defensive(initial_budget, '2016-01-11', '2017-12-29'))
    list_of_aggressive_investors.append(strat_aggressive(initial_budget, '2016-01-11', '2017-12-29'))
    list_of_mixed_investors.append(strat_mixed(0.25, 0.75, initial_budget, '2016-01-11', '2017-12-29'))

# investment made from 2016 to 2018, Return for 3 years
for i in range(models3):
    year.append('2018')
    list_of_defensive_investors.append(strat_defensive(initial_budget, '2016-01-11', '2018-12-31'))
    list_of_aggressive_investors.append(strat_aggressive(initial_budget, '2016-01-11', '2018-12-31'))
    list_of_mixed_investors.append(strat_mixed(0.25, 0.75, initial_budget, '2016-01-11', '2018-12-31'))


# investment made from 2016 to 2019, Return for 4 years
for i in range(models4):
    year.append('2019')
    list_of_defensive_investors.append(strat_defensive(initial_budget, '2016-01-11', '2019-12-31'))
    list_of_aggressive_investors.append(strat_aggressive(initial_budget, '2016-01-11', '2019-12-31'))
    list_of_mixed_investors.append(strat_mixed(0.25, 0.75, initial_budget, '2016-01-11', '2019-12-31'))

#  investment made from 2016 to 2020, Return for 5 years
for i in range(models5):
    year.append('2020')
    list_of_defensive_investors.append(strat_defensive(initial_budget, '2016-01-11', '2020-12-31'))
    list_of_aggressive_investors.append(strat_aggressive(initial_budget, '2016-01-11', '2020-12-31'))
    list_of_mixed_investors.append(strat_mixed(0.25, 0.75, initial_budget, '2016-01-11', '2020-12-31'))

Investor_modelling_data = pd.DataFrame({'Year': year,
                                      'Defensive': list_of_mixed_investors,
                                      'Aggressive': list_of_aggressive_investors,
                                      'Mixed': list_of_mixed_investors})

print(Investor_modelling_plot)

defensive_mean = []
aggressive_mean =[]
mixed_mean = []

# Create a dataframe with the yearly mean for each type of investors
years = ['2016', '2017', '2018', '2019', '2020']
for i in years:
    investors = Investor_modelling_data[Investor_modelling_data['Year'] == i]
    defensive_mean.append(investors['Defensive'].mean())
    aggressive_mean.append(investors['Aggressive'].mean())
    mixed_mean.append(investors['Mixed'].mean())

mean_return = pd.DataFrame({'Year': years,
                           'Defensive': defensive_mean,
                           'Aggressive': aggressive_mean,
                           'Mixed': mixed_mean})
print(mean_return)

# Plot the values for every year over the period
Mean_return_plot = mean_return.plot()
plt.xlabel('Year')
plt.ylabel('Return')
plt.title('Return plot over 5 years of investment')
plt.show()
Mean_return_plot.figure.savefig(os.path.abspath("../Return plot over 5 years of investment.png"))
