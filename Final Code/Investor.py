from BondsClasses import Term.SHORT, Term.LONG
from stocks import Data_imported, Stocks
import matplotlib.pyplot as plt
import os
import random

initial_budget = 1000
class Investor(object):
    def __init__(self, budget):
        self.budget = budget

class Defensive(Investor):
    def strat_defensive(self, remaining_budget, start_date, end_date):
        remaining_budget = 0
        start_date = input(print('date of the investment'))
        end_date = input(print('date of end of investment'))
        while (self.initial_budget <= 200) and (remaining_budget >= 200):
            The_chosen_bond = random.choice(Term)
            Term.amount = 0.5 * self.initial_budget
            remaining_budget = self.initial_budget - Term.amount
            return remaining_budget

class Aggressive(Investor):
    def strat_aggressive(self,date, start_date, end_date):
        stocks = ['FDX', 'GOOG', 'IBM', 'KO', 'MSFT', 'NOK', 'XOM']
        start_date = input(print('date of the investment'))
        end_date = input(print('date of end of investment'))
        first_choice = str(random.choice(stocks))
        price = Data_imported.loc[date, first_choice]
        print(first_choice)
        print(price)
        remaining_budget = self.initial_budget - price
        while remaining_budget >= 100:
            possible_choice = []
            for i in stocks:
                if Data_imported.loc[date, i] < remaining_budget:
                    possible_choice.append(i)
                    chosen_stock = str(random.choice(possible_choice))
                    price = Data_imported.loc[date, chosen_stock]
                    remaining_budget -= price
                return remaining_budget and possible_choice

class mixed(Investor):
    def strat_mixed(self, remaining_budget, stocks, date, prob_bonds, prob_stocks, start_date, end_date):
        investment_bonds = prob_bonds* initial_budget
        investment_stocks = prob_stocks * initial_budget
        investment_bonds = 0.5 * Term.amount + 0.5 * Term.amount
        while remaining_budget >= 200:
            possible_choice = []
            for i in stocks:
                if Data_imported.loc[date, i] < remaining_budget:
                    possible_choice.append(i)
                    chosen_stock = str(random.choice(possible_choice))
                    price = Data_imported.loc[date, chosen_stock]
                    remaining_budget -= price
                return remaining_budget


list_of_defensive_investors=[]
list_of_aggressive_investors = []
list_of_mixed_investors = []

for i in range(500):
    date = random.choice(Data_imported['Date'])
    list_of_defensive_investors.append(mixed.strat_defensive(0.25, 0.75, 5000, date))
    list_of_aggressive_investors.append(Aggressive.strat_aggressive(5000, date))
    list_of_mixed_investors.append(Defensive.strat_defensive(5000, date))

# Plotting the date
## Plot the modelisation in order to analyse it
fig = plt.figure(3)
plt.plot(list_of_aggressive_investors,
         list_of_defensive_investors,
         list_of_mixed_investors)
plt.xlabel('Models')
plt.ylabel('Return')
plt.title('Investor modelling')
plt.grid(True)
plt.show()
fig.savefig(os.path.abspath(("../Results/Investors.png")))



ShortBond1 = Term.SHORT(200,20,0.015).min_price
LongBond1 = Term.LONG(1000,13,0.03).min_price
Investor_1 = Defensive(initial_budget).strat_defensive(ShortBond1, LongBond1)
print(Investor_1)

Stocks = ['FDX', 'GOOG', 'IBM', 'KO', 'MS', 'NOK', 'XOM']
Investor_2 = Aggressive(initial_budget).strat_aggressive("2016-01-08")
print(Investor_2)
Data_imported.reset_index(inplace=True)
