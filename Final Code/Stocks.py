import yfinance as yf
import matplotlib.pyplot as plt
import os
# target tickers
All_tickers = ['FDX', 'GOOG', 'IBM', 'KO', 'MSFT', 'NOK', 'XOM']

# download the series from Yahoo finance, with a few more days at the
# beginning to deal with missing values and returns
Data_imported = yf.download(All_tickers, start='2016-01-09', end='2021-01-01')['High']

# forward-fill missing values
#df = df.fillna(method='ffill')
#print(df)
Data_imported.dropna()

Data_imported = Data_imported.resample('D').bfill().reset_index()
print(Data_imported)
Data_imported.set_index('Date', inplace = True)
#Plotting stock prices
Fig = Data_imported[['FDX', 'GOOG', 'IBM', 'KO', 'MSFT', 'NOK', 'XOM']] \
    .plot(figsize=(10, 6), grid=True, title='Daily Stock Prices over time', legend=False)
Fig.set_ylabel('Prices')
plt.savefig(os.path.abspath('../Plotting all the stocks'))
plt.show()

#Computing daily returns
All_stocks_data = Data_imported[All_tickers].pct_change()
All_stocks_data.dropna()
#df_merged_returns['Date'] = pd.date_range(start="2016-01-09", end="2021-01-01")
print(All_stocks_data)

#Plotting daily returns

All_stocks_data.plot()
plt.xlabel('Business days')
plt.ylabel('Returns')
plt.title('Return on investment')
plt.show()

## creating the class stocks
def nearest(self, df, date):
    return min(df, key=lambda x: abs(x - date))

class Stocks(object):
    def __init__(self, t, amount, name, number_of_stocks_bought, purchase_date):
        self.t = t
        self.amount = amount
        self.number_of_stocks_bought = number_of_stocks_bought
        self.name = ['FDX', 'GOOGL', 'XOM', 'KO','NOK', 'MS', 'IBM']
        self.purchase_date = purchase_date

    def purchase_date_accepted(self):
        if self.purchase_date in All_stocks_data['Date']:
            return("This date is available")
        else:
            return ValueError("Enter a valid purchase date")

    def get_the_price(self, name, start_date, end_date):
        start_date = input(print('Give a start date for the investment'))
        end_date = input(print('Give an end date for your investment'))
        if (start_date in All_stocks_data['Date']) and (end_date in All_stocks_data['Date']):
            price_start = All_stocks_data[(All_stocks_data['DATE'] == start_date) & (All_stocks_data['NAME'] == name)]
            price_end = All_stocks_data[(All_stocks_data['DATE'] == end_date) & (All_stocks_data['NAME'] == name)]
            return price_start & price_end
        elif (start_date in All_stocks_data['Date']) or (end_date in All_stocks_data['Date']):
            start_date = nearest(All_stocks_data['Date'], start_date)
            end_date = nearest(All_stocks_data['Date'], end_date)
            price_start = All_stocks_data[(All_stocks_data['DATE'] == start_date) & (All_stocks_data['NAME'] == name)]
            price_end = All_stocks_data[(All_stocks_data['DATE'] == end_date) & (All_stocks_data['NAME'] == name)]
            return price_start & price_end
